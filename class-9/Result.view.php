<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="bootstrap.min.css" />
</head>
<body>
    <div class="container">
        <table class="table table-bordered table-primary">
            <tr>
                <th>Name</th>
                <th>Age</th>
                <th>HTML</th>
                <th>PHP</th>
                <th>Grade</th>
            </tr>
            <?php 

                foreach(Result::studnets() as $student):
                    
                if ($student['php'] == false) 
                continue;

            ?>
                <tr class="<?= Result::valided_data($student['grade']); ?> font-weight-bold">
                    <td><?= $student['name']; ?></td>
                    <td><?= $student['age']; ?></td>
                    <td><?= Result::checker($student['html']) ; ?></td>
                    <td><?= Result::checker($student['php']) ; ?></td>
                    <td><?= Result::grade($student['grade']) ; ?></td>
                </tr>
            <?php endforeach;?>
        </table>
    </div>
</body>
</html>