
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="bootstrap.min.css" />
    <style>
        .container{
            width: 500px;
            margin-top: 50px;
        }
    </style>
</head>
<body>
    <div class="container">
        <table class="table table-bordered border-primary text-center">
            <thead class="table-primary">
                <tr>
                    <th>Name</th>
                    <th>Roll</th>
                    <th>REG</th>
                    <th>PHP</th>
                    <th>GRADE</th>
                </tr>
            </thead>
            <tbody>
            <?php 
                foreach( Stduents::students() as $student):
            ?>
                <tr>
                    <td><?= $student['name']; ?></td>
                    <td><?= $student['age']; ?></td>
                    <td><?= $student['html']; ?></td>
                    <td><?= $student['php']; ?></td>
                    <td><?= $student['grade']; ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</body>
</html>