<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body"> 
                        <form action="index.php" method="POST">
                            <div class="form-group">
                                <label for="class">Select Class</label>
                                <select name="class" id="class" class="form-control">
                                    <option value="">Select One</option>
                                    <?php foreach($clsses as $class): ?>
                                        <option value="<?= $class['cls_id']; ?>"><?= strtoupper($class['cls_name']) ; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <button name="submit_btn" type="submit" class="btn btn-success">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div class="container mt-4">
        <table class="table table-bordered">
            <thead class="table-success">
                <tr>
                    <th>SL NO</th>
                    <th>Name</th>
                    <th>Class</th>
                    <th>Roll</th>
                    <th>REG</th>
                    <th>HTML</th>
                    <th>PHP</th>
                    <th>Grade</th>
                </tr>
            </thead>

            <tbody>
<?php 

if (isset($_POST['submit_btn'])) {
    if($rowCount > 0):
        foreach ($students as $student):
            

    ?>
                    <tr>
                        <td><?= $i; ?></td>
                        <td><?=$student['std_name']?></td>
                        <td><?=$student['cls_name']?></td>
                        <td><?=$student['std_roll']?></td>
                        <td><?=$student['std_reg']?></td>
                        <td><?=$student['html']?></td>
                        <td><?=$student['php']?></td>
                        <td><?=$student['grade']?></td>
                    </tr>
<?php
        $i++;
        endforeach;
    else:
        echo "<tr><td class='text-danger text-center' colspan='8'><h2> No data here</h2></td></tr>";
    endif;

}
 
 ?>
            </tbody>

        </table>
    </div>
</body>
</html>