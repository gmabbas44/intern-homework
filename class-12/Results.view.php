<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="bootstrap.min.css" />
</head>
<body>
    <div class="container">
        <table class="table table-bordered table-primary">
            <tr>
                <th>Name</th>
                <th>Age</th>
                <th>HTML</th>
                <th>PHP</th>
                <th>Grade</th>
            </tr>
            <?php 

                foreach($results->all()->fetch_assoc() as $student):
                    var_dump($student);
            ?>
                <tr class="<?php /*Result::valided_data($student['grade']);*/ ?> font-weight-bold">
                    <td><?= $student['std_name']; ?></td>
                    <td><?= $student['std_cls_id']; ?></td>
                    <td><?= $student['html'] ; ?></td>
                    <td><?= $student['php'] ; ?></td>
                    <td><?= $student['grade'] ; ?></td>
                </tr>
            <?php endforeach;?>
        </table>
    </div>
</body>
</html>