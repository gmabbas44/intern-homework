<?php
include 'function.php';

$con = mysqli_connect('localhost', 'root', '', 'r_creation');

$sql = "SELECT * FROM class";
$result = mysqli_query($con, $sql);

$classes = mysqli_fetch_all($result, MYSQLI_ASSOC);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css"/>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card bg-light">
                    <div class="card-body">
                        <form action="">
                            <div class="form-group">
                                <label for="Search">Select Class</label>
                                <select class="form-control" name="classes" id="classes" onchange="classChange(this.value)">
                                    <option value="0">All</option>
                                    
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container mt-4">
        <div id="showStudent">
            
        </div>
    </div>
    
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.min.js"></script>



<script>

$(document).ready(function(){
    classChange(0);

    var list = [
        <?php foreach ($classes as $id => $class):
              $s =  strtoupper($class['cls_name']);
              echo "'".$s."',";
            endforeach;
        ?>
    ];
    $('#classes').select2({
        data:list,
        value : 
    });

});


function classChange(id){
    var id = id;

    $.ajax({
        url : 'table-data.php',
        type : 'post',
        data : { class_id : id},

        success :function(data, status){
            $('#showStudent').html(data);
        }
    });

}




</script>
</body>
</html>