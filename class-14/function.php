<?php
$con = mysqli_connect('localhost', 'root', '', 'r_creation');

function grade($data){
    if($data >= 0.00 && $data <= 4.00){
        if ($data == 4) {
            $g = 'A+';
        } else if($data >= 3.75) {
            $g = 'A';
        } else if($data >= 3.50) {
            $g = 'A-';
        } else if($data >= 3.25) {
            $g = 'B+';
        } else if($data >= 3.00) {
            $g = 'B';
        } else if($data >= 2.75) {
            $g = 'B-';
        } else if($data >= 2.50) {
            $g = 'C+';
        } else if($data >= 2.25) {
            $g = 'C';
        } else if($data >= 2.00) {
            $g = 'D';
        } else if($data >= 0.00) {
            $g = 'F';
        }
    }else{
        $g = "Wrong Input";
    }
    return $g;
}

function checker($data){
    return $data == true ? '&#10003;' : '&#10005;';
}

function valided_data($grade){
    if($grade < 2.00 || $grade > 4.00){
        $color = 'bg-danger text-white';
    }  
    else if ($grade == 4){ 
        $color = 'bg-success text-white';
    }
    else{
        $color = '';
    }  
    return $color;
}

function query($class_id){
    global $con;
    $sql = "SELECT std_id, std_name, cls_name, std_roll, std_reg, html, php, grade FROM student JOIN class ON std_cls_id = cls_id WHERE std_cls_id = $class_id";
    return mysqli_query($con, $sql);
}
function all(){
    global $con;
    $sql = "SELECT std_id, std_name, cls_name, std_roll, std_reg, html, php, grade FROM student JOIN class ON std_cls_id = cls_id";
    return mysqli_query($con, $sql);
}

function escapeString($data){
    global $con;
    $data = trim($data);
    $data = htmlspecialchars($data);
    $data = mysqli_real_escape_string($con, $data);
    return $data;
}