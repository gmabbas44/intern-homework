<?php
include_once 'function.php';

$class_id  = escapeString($_POST['class_id']);

if(isset($class_id) && is_numeric($class_id)):

    $result = ($class_id == 0) ? all() : query($class_id);

    if(mysqli_num_rows($result) > 0 ):
        $students = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $i = 1

?>

<table class="table table-bordered">
    <thead class="table-primary text-center">
        <th>SL</th>
        <th>Name</th>
        <th>Class</th>
        <th>Roll</th>
        <th>Reg</th>
        <th>HTML</th>
        <th>PHP</th>
        <th>Grade</th>
    </thead>
    <tbody>
<?php foreach ($students as $student): ?>
        <tr>
            <td><?= $i; ?></td>
            <td><?= escapeString($student['std_name']); ?></td>
            <td><?= strtoupper($student['cls_name']); ?></td>
            <td><?= escapeString($student['std_roll']); ?></td>
            <td><?= escapeString($student['std_reg']); ?></td>
            <td><?= checker(escapeString($student['html'])); ?></td>
            <td><?= checker(escapeString($student['php'])); ?></td>
            <td class="<?=valided_data($student['grade']);?>"><?= escapeString(grade($student['grade'])); ?></td>
        </tr>
<?php $i++; endforeach;?> 
    </tbody>
</table>

<?php
    else:
        echo "<h2 class='text-danger text-center'> No data here</h2>";
    endif;
endif;
?>