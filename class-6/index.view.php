<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <table class="table table-bordered table-primary">
            <tr>
                <th>Name</th>
                <th>Age</th>
                <th>HTML</th>
                <th>PHP</th>
                <th>Grade</th>
            </tr>
            <?php foreach($student_info as $student): ?>
                <?php 
                    if($student['grade'] < 2.00 || $student['grade'] > 4.00){
                        $color = 'bg-danger';
                        $text = 'text-white';
                    }  
                    else if ($student['grade'] == 4){
                        $color = 'bg-success';
                        $text = 'text-white';
                    }
                    else{
                        $text = '';
                        $color = '';
                    }
                        

                    if ($student['php'] == false) 
                    continue;
                ?>
                <tr class="<?= $color.' '. $text; ?> font-weight-bold">
                    <td><?= $student['name']; ?></td>
                    <td><?= $student['age']; ?></td>
                    <td><?= checker($student['html']) ; ?></td>
                    <td><?= checker($student['php']) ; ?></td>
                    <td><?= grade($student['grade']) ; ?></td>
                </tr>
            <?php endforeach?>
        </table>
    </div>
</body>
</html>