<?php
	function checker($d){
		return $d == true ? '&#10003;' : '&#10005;'; 
	}
	function grade($r){
		if ($r==5)
				return 'A+'; 
		elseif ($r >= 4 && $r <= 4.99) 
				return 'A';
		elseif ($r >= 3.5 && $r <= 3.99)
				return 'A-';
		elseif ($r >= 3 && $r <= 3.49) 
				return 'B';
		elseif ($r >= 2 && $r <= 2.99)
				return 'C';
		elseif ($r >= 1 && $r <= 1.99)
				return 'D';
		else
			return 'F';

	}
	$student_info = [
					[
						'name'=> 'Md Abbas Uddin',
						'age'=> 60,
						'html'=> true,
						'php'=> true,
						'grade'=> 0.1
					],
					[
						'name'=> 'Khaleda Akter',
						'age'=> 50,
						'html'=> true,
						'php'=> true,
						'grade'=> 4
					],
					[
						'name'=> 'Md Anwar Ullah',
						'age'=> 40,
						'html'=> false,
						'php'=> false,
						'grade'=> 3.5
					],

				];
 
 	require 'index.view.php';