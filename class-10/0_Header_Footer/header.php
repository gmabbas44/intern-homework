<!DOCTYPE html>
<html>
<head>
	<title>Practice OOP</title>
	<style type="text/css">
		*{
			margin: 0;
			padding: 0;
		}
		body{
			width: 980px;
			margin: 0 auto;
		}
		.continer{
			background: rgba(59, 143, 225, 1);
			color: #fff;
			font-family: arial;
		}
		.header, .footer{
			text-align: center;
			padding: 15px;
			border: 5px solid #fff;
		}
		.header h2{
			font-size: 2.3em;
		}
		.footer p{
			font-size: 1em;
		}
		.section{
			border: 5px solid #fff;
			min-height: 450px;
		}
		.maincontain{
			margin: 4px;
			background: #fff;
			min-height: 450px;
			padding: 10px;
			color: #000;
			font-size: 1.5em;
			font-family: arial;
		}

	</style>
</head>
<body>
	<div class="continer">
		<header class="header">
			<h2>Practice OPP</h2>
		</header>
		<section class="section">
			<div class="maincontain">