<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Nested Loop</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<style>
		.fix{
			width: 25%;
			margin: 20px auto;
		}
	</style>
</head>
<body>
<div class="fix">
	<?php
		
		$n = 10;
		for($i = 1; $i<= $n; $i++):
			echo "<table class='table table-bordered table-sm text-center'>";
			for($j = 1; $j<= 10; $j++):
		?>
			<tr>
				<td><?= $i; ?></td><td>*</td><td><?= $j; ?></td><td>=</td><td><?=  $i*$j ;?></td>
			</tr>
		<?php
			endfor;
			echo '</table>';
			echo '<br>';
		endfor;

	?>
</div>
</body>
</html>